package Queue;

import Stacks.Node;

public class Queue {
	private Node head;
	private int size;

	public Queue() {
		this.setHead(null);
		this.setSize(0);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Node getHead() {
		return head;
	}

	public void setHead(Node head) {
		this.head = head;
	}

	public void push(int d) {
		if (head == null)
			head = new Node(d);
		else {
			Node temp = head;
			while (temp.getNext() != null)
				temp = temp.getNext();
			temp.setNext(new Node(d));
		}
		this.size++;
	}

	public void pop() {
		if (head == null)
			System.out.println("queue is empty");
		else {
			head = head.getNext();
			this.size--;
		}
	}

	public void traverse() {
		Node temp = head;
		if (head == null)
			System.out.println("queue is empty");
		else
			while (temp != null) {
				System.out.println(temp.getData());
				temp = temp.getNext();
			}
	}

	public void peek() {
		Node temp = head;
		if (head == null)
			System.out.println("queue is empty");
		else {
			while (temp.getNext() != null)
				temp = temp.getNext();
			System.out.println(temp.getData());
		}

	}

}
