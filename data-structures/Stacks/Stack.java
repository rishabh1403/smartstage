package Stacks;

public class Stack {

	private int size;
	private Node head;

	public Stack() {
		this.head = null;
		this.size = 0;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Node getHead() {
		return head;
	}

	public void setHead(Node head) {
		this.head = head;
	}

	public void push(int d) {
		if (head == null)
			head = new Node(d);
		else {
			Node temp = head;
			while (temp.getNext() != null)
				temp = temp.getNext();
			temp.setNext(new Node(d));
		}
		this.size++;
	}

	public void traverse() {
		Node temp = head;
		if (head == null)
			System.out.println("stack is empty");
		else
			while (temp != null) {
				System.out.println(temp.getData());
				temp = temp.getNext();
			}
	}

	public void pop() {
		Node temp = head;
		if (head == null)
			System.out.println("stack is empty");
		else if (head.getNext() == null)
			head = null;
		else {
			while (temp.getNext() != null && temp.getNext().getNext() != null)
				temp = temp.getNext();
			temp.setNext(null);
		}
		size--;
	}

	public void peek() {
		Node temp = head;
		if (head == null)
			System.out.println("stack is empty");
		else {
			while (temp.getNext() != null)
				temp = temp.getNext();
			System.out.println(temp.getData());
		}

	}

}
