'use strict';

var app = angular.module('todo', []);


app.controller('TodoController', TodoController);

function TodoController($scope, $http) {
	$scope.newTask = "my first task";

	$scope.submitTask = function() {
		console.log('submitting task', $scope.newTask);
		$scope.tasks.push({name: $scope.newTask});
	}

	$scope.getCompletedTasks = function() {
		if ($scope.tasks === undefined) return 0;
		var completedTasks = $scope.tasks.filter(function (task) {
			return task.completed;
		});
		return completedTasks.length;
	}

	$scope.init = function() {
		var request = {
			method: 'GET',
			url: '/tasks',
		}
		$http(request)
			.success(function(data) {
				$scope.tasks = data;
			})
			.error(function(data) {
				console.error(data);
			});
	}

}
