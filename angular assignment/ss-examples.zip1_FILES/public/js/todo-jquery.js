$(document).on('ready', getStarted);

/**
 * All actions to performed once the document is ready!
 */
function getStarted() {
	notifyMe();
	loadArchivedTasks();
	registerOnClickActionForSubmitBtn();
	registerOnClickEventForTasksCheckbox();
	registerOnClickEventForArchiveTasksBtn();
}

////////////

function archiveTasks() {
	var allTasks = $('ul#tasks').children('li.task');

	var tasks = [];
	for (var i=0; i<allTasks.length; i++) {
		var aTask = $(allTasks[i]);
		var check = aTask.children('.toggle-complete');

		var taskName = $(allTasks[i]).text();
		var completed = $(check[0]).is(':checked');

		var task = {
			name: taskName, completed: completed
		}
		tasks.push(task);
	}

	$.ajax({
		url: '/tasks',
		type: 'POST',
		dataType: 'json',
		data: JSON.stringify({tasks: tasks}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.done(function() {
		console.log("success");
		alert('tasks archived');
	})
	.fail(function() {
		console.log("error");
	});
	
}

function appendTask(task, completed) {
	$('ul#tasks').append( 
		[
			'<li ' + addClass(completed) + '>',
				'<input type="checkbox"'
					+ ' class="toggle-complete"' 
					+ ' ' + addChecked(completed) 
					+ ' />',
				task,
			'</li>'
		].join('')
	);

	function addClass(completed) {
		return 'class="task' + (completed ? ' complete' : '') + '"';
	}

	function addChecked(completed) {
		return (completed ? 'checked' : '');
	}
}

function loadArchivedTasks() {
	$.ajax({
		url: '/tasks',
		type: 'GET',
		dataType: 'json'
	})
	.done(function(tasks) {
		tasks.forEach(function(task) {
			appendTask(task.name, task.completed);
		});
	})
	.fail(function() {
		console.log("error");
	});
	
}

function markTaskComplete(event) {
	var toggleSwitch = $(this);
	var task = toggleSwitch.parent('.task');
	console.log('clicked checkbox for', task.text());

	task.toggleClass('complete');
}

function notifyMe() {
	console.log('Document is ready!');
}

function registerOnClickEventForArchiveTasksBtn() {
	$('button#archiveTaskBtn').on('click', archiveTasks);
}

function registerOnClickActionForSubmitBtn() {
	$('button#submitTaskBtn').on('click', submitTask);
}

function registerOnClickEventForTasksCheckbox() {
	$('ul#tasks').on('click', 'li.task input.toggle-complete', markTaskComplete);
}

function submitTask(event) {
	console.log('submitting task');

	var task = $('input#task').val();
	if (task === undefined || task === '') {
		alert('Please enter a task!');
	} else {
		appendTask(task, false);
	}
}
