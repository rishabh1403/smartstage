'use strict';

var app = angular.module('todo', []);
app.controller('TodoController', TodoController);
													
function TodoController($scope, $http) {        
	$scope.init = function() {       
		var request = {				
			method: 'GET',               
			url: '/tasks',
		}
		$http(request)
			.success(function(data) {
				$scope.tasks = data;
			})
			.error(function(data) {
				console.error(data);
			});
	}

	$scope.newTask = "my first task"; 
	$scope.submitTask = function() {
		console.log('submitting task', $scope.newTask);
		$scope.tasks.push({name: $scope.newTask});        
	}													 

	$scope.getCompletedTasksCount = function() {
		if ($scope.tasks === undefined) return 0;
		var Count = $scope.tasks.filter(function (task) {        
			return task.completed==true;                               
		});														
		return Count.length;      
	}
	
	$scope.completeAll = function() {
		console.log('completing all task');
		angular.forEach($scope.tasks,function(value,index){
                value.completed=true;
            });
	}	

	$scope.archiveTasks = function() {
		console.log('Archiving all task');
		var request = {				
			method: 'POST',               
			url: '/tasks',
			data: {tasks:$scope.tasks}
		}
		$http(request)
			.success(function() {
				console.log("success");
				alert('tasks archived');
			})
			.error(function() {
				console.log("error");
			});
	}

	/////////////////////////////////////////////////////////////////////////////////

}
