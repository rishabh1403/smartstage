'use strict'

var express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	http = require('http').Server(app);

var app = express();

app.use(bodyParser.json());

app.get('/data', function(req, res) {
	res.send({
		data: [
			'apples',
			'oranges',
			'watermelon',
			'grapes'
		]
	})
});

var tasks = [];
app.get('/tasks', function(req, res) {
	res.send(tasks);
});

app.post('/tasks', function(req, res) {
	tasks = req.body.tasks;
	res.send({success: true});
});

app.use(express.static('./public/'));
app.use(express.static('./'));

app.listen(7777, function() {
	console.log('started server on port 7777');
});